ARG ALPINE_VERSION=3.21
FROM alpine:${ALPINE_VERSION}
LABEL maintainer="shane@northernv.com"
LABEL org.opencontainers.image.source="https://gitlab.com/northernv/dockerfiles/psql.git"
ARG PG_VERSION=16
RUN apk update && \
    apk upgrade && \
    apk add --no-cache \
    curl \
    jq \
    bash \
    git \
    postgresql${PG_VERSION}-client \
    coreutils
RUN apk add dbmate --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing/
ENTRYPOINT [ "psql" ]